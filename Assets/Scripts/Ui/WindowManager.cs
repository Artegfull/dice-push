using System.Collections.Generic;
using Ui.Windows;
using UnityEngine;
using Zenject;


namespace Ui
{
	public class WindowManager : MonoBehaviour
	{
		private readonly List<BaseWindow> shownWindows = new List<BaseWindow>();

		[Inject] private DiContainer container;
		
		public BaseWindow ShowWindow(BaseWindow windowPrefab)
		{
			Hide();

			BaseWindow window = container.InstantiatePrefabForComponent<BaseWindow>(windowPrefab, transform);
			
			window.Show();
			shownWindows.Add(window);

			return window;
		}


		public void Hide()
		{
			foreach (BaseWindow window in shownWindows)
			{
				window.Hide();
			}

			shownWindows.Clear();
		}
	}
}
