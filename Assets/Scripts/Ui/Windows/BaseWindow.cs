using UnityEngine;


namespace Ui.Windows
{
	public class BaseWindow : MonoBehaviour
	{
		public virtual void Show() { }
		
		
		public virtual void Hide() { }
	}
}
