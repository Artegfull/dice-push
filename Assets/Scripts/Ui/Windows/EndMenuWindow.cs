using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utility;
using Zenject;


namespace Ui.Windows
{
	public class EndMenuWindow : BaseWindow
	{
		#region Fields

		[SerializeField] private Button restart;
		[SerializeField] private RectTransform titleTransform;
		[SerializeField] private TextMeshProUGUI title;
		[SerializeField] private float showDuration;
		[SerializeField] private int titleYMovement;

		[SerializeField] private string winText;
		[SerializeField] private string loseText;

		private Vector3 hiddenTitlePosition;

		#endregion

		

		#region Injection

		[Inject] private SignalBus signalBus;
		[Inject] private WindowManager windowManager;

		#endregion



		#region Unity lifecycle

		private void Start()
		{
			restart.onClick.AddListener(() =>
			{
				signalBus.Fire<RestartGameSignal>();
				windowManager.Hide();
			});
		}


		private void OnDestroy()
		{
			DOTween.Kill(this);
		}

		#endregion



		#region Methods

		public void SetWinner(bool isPlayerWon)
		{
			title.text = isPlayerWon ? winText : loseText;
		}
		
		
		public override void Show()
		{
			Vector3 initialScale = restart.transform.localScale;
			
			restart.transform.localScale = Vector3.zero;
			restart.transform.DOScale(initialScale, showDuration).SetId(this);

			Vector3 initialPosition = titleTransform.position;
			
			hiddenTitlePosition = titleTransform.TransformPoint(Vector3.up * titleYMovement);
			titleTransform.transform.position = hiddenTitlePosition;
			titleTransform.DOMove(initialPosition, showDuration).SetId(this);
		}


		public override void Hide()
		{
			restart.transform.DOScale(Vector3.zero, showDuration).OnComplete(() => Destroy(gameObject)).SetId(this);
			titleTransform.DOMove(hiddenTitlePosition, showDuration).SetId(this);
		}

		#endregion
	}
}
