using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


namespace Ui.Windows
{
	public class StartMenuWindow : BaseWindow
	{
		#region Fields

		[SerializeField] private Button startButton;
		[SerializeField] private RectTransform titleTransform;
		[SerializeField] private float showDuration;
		[SerializeField] private int titleYMovement;

		private Vector3 hiddenTitlePosition;

		#endregion



		#region Injection

		[Inject] private SignalBus signalBus;
		[Inject] private WindowManager windowManager;

		#endregion


		
		#region Unity lifecycle

		private void Start()
		{
			startButton.onClick.AddListener(() =>
			{
				signalBus.Fire<StartGameSignal>();
				windowManager.Hide();
			});
		}


		private void OnDestroy()
		{
			DOTween.Kill(this);
		}

		#endregion



		#region Methods

		public override void Show()
		{
			Vector3 initialScale = startButton.transform.localScale;
			
			startButton.transform.localScale = Vector3.zero;
			startButton.transform.DOScale(initialScale, showDuration).SetId(this);

			Vector3 initialPosition = titleTransform.position;
			
			hiddenTitlePosition = titleTransform.TransformPoint(Vector3.up * titleYMovement);
			titleTransform.transform.position = hiddenTitlePosition;
			titleTransform.DOMove(initialPosition, showDuration).SetId(this);
		}


		public override void Hide()
		{
			startButton.transform.DOScale(Vector3.zero, showDuration).OnComplete(() => Destroy(gameObject)).SetId(this);
			titleTransform.DOMove(hiddenTitlePosition, showDuration);
		}

		#endregion
	}
}
