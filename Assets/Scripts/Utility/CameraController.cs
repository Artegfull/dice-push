using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


namespace Utility
{
    public class CameraController : MonoBehaviour
    {
        #region Fields

        private const float NORMAL_RESOLUTION = 1920f / 1080f;

        [SerializeField] private Vector3 shakeForce;
        [SerializeField] private float shakeDuration;
        [SerializeField] private new Camera camera;

        #endregion



        #region Unity lifecycle

        private void Awake()
        {
            float currentResolution = (float) Screen.height / Screen.width;
            float ratio = currentResolution / NORMAL_RESOLUTION;
            
            ScaleLinearly(camera, ratio);
        }

        #endregion



        #region Methods

        public void Shake()
        {
            camera.DOShakePosition(shakeDuration, shakeForce).SetId(this);
        }


        private void ScaleLinearly(Camera cameraForScale, float ratio)
        {
            cameraForScale.fieldOfView *= ratio;
        }

        #endregion
    }
}
