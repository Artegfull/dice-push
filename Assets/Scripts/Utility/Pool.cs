using System.Collections.Generic;
using UnityEngine;


namespace Utility
{
	public class Pool<T> where T : MonoBehaviour
	{
		#region Fields

		private readonly T prefab;
		private readonly Queue<T> instances;

		#endregion



		#region Methods

		public Pool(T prefab, int initialSpawnedCount)
		{
			this.prefab = prefab;
			instances = new Queue<T>();
			
			PopulatePool(initialSpawnedCount);
		}


		public virtual MonoBehaviour Create()
		{
			T instance = instances.Count > 0 ? instances.Dequeue() : Instantiate();
			ToggleVisibility(instance, true);

			return instance;
		}


		public void Free(T instance)
		{
			ToggleVisibility(instance, false);
			
			instances.Enqueue(instance);
		}
		
		
		protected virtual T Instantiate()
		{
			T instance = Object.Instantiate(prefab);
			ToggleVisibility(instance, false);

			return instance;
		}


		private void PopulatePool(int amount)
		{
			for (int i = 0; i < amount; i++)
			{
				instances.Enqueue(Instantiate());
			}
		}


		private void ToggleVisibility(T instance, bool isEnabled)
		{
			instance.gameObject.SetActive(isEnabled);
		}

		#endregion
	}
}
