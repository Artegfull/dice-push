using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Utility
{
    [Serializable]
    public class MinMaxVector
    {
        #region Fields

        [SerializeField] private Vector3 maxValue;
        [SerializeField] private Vector3 minValue;

        #endregion



        #region Methods

        public Vector3 GetClamped(Vector3 value) => new Vector3(
            Mathf.Clamp(value.x, minValue.x, maxValue.x),
            Mathf.Clamp(value.y, minValue.y, maxValue.y),
            Mathf.Clamp(value.z, minValue.z, maxValue.z)
        );


        public Vector3 GetRandom() => new Vector3(
            Random.Range(minValue.x, maxValue.x),
            Random.Range(minValue.y, maxValue.y),
            Random.Range(minValue.z, maxValue.z)
        );
    
    
        public Quaternion GetClampedQuaternion(Quaternion quaternion)
        {
            quaternion.x /= quaternion.w;
            quaternion.y /= quaternion.w;
            quaternion.z /= quaternion.w;
            quaternion.w = 1.0f;
 
            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(quaternion.x);
            angleX = Mathf.Clamp(angleX, minValue.x, maxValue.x);
            quaternion.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
 
            float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(quaternion.y);
            angleY = Mathf.Clamp(angleY, minValue.y, maxValue.y);
            quaternion.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);
 
            float angleZ = 2.0f * Mathf.Rad2Deg * Mathf.Atan(quaternion.z);
            angleZ = Mathf.Clamp(angleZ, minValue.z, maxValue.z);
            quaternion.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleZ);
 
            return quaternion;
        }

        #endregion
    }
}
