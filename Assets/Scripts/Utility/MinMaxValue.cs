using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Utility
{
    [Serializable]
    public class MinMaxValue
    {
        [SerializeField] private float minValue;
        [SerializeField] private float maxValue;


        public float GetRandom()
        {
            return Random.Range(minValue, maxValue);
        }
    }
}
