using DG.Tweening;
using UnityEngine;


namespace Utility
{
	public class Popup : MonoBehaviour
	{
		#region Fields

		[SerializeField] private float showDuration;
		[SerializeField] private float hideDuration;
		[SerializeField] private float yMovement;

		#endregion



		#region Unity lifecycle

		private void Start()
		{
			DOVirtual.DelayedCall(showDuration, Hide);
			transform.DOMoveY(transform.position.y + yMovement, showDuration + hideDuration).SetId(this);
		}


		private void OnDestroy()
		{
			DOTween.Kill(this);
		}

		#endregion



		#region Methods

		private void Hide()
		{
			transform.DOScale(Vector3.zero, hideDuration).SetId(this).onComplete += () => Destroy(gameObject);
		}

		#endregion
	}
}
