using System.Collections.Generic;
using UnityEngine;


namespace Utility
{
    public class EffectHandler : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> particleSystems;
        [SerializeField] private float destroyDelay = 3f;
        

        public void PlayEffect()
        {
            foreach (ParticleSystem system in particleSystems)
            {
                system.Play(true);
            }

            Destroy(gameObject, destroyDelay);
        }
    }
}
