using System;
using Dice;
using Npc;
using Npc.NpcStates;
using Ui.Windows;
using UnityEngine;
using Utility;
using Zenject;
using Factory = Dice.Factory;


namespace Cannon
{
	public class CannonInstaller : MonoInstaller<CannonInstaller>
	{
		#region Fields

		[SerializeField] private Cannon cannon;
		[SerializeField] private CannonController controller;
		[SerializeField] private TrajectoryPredictor trajectoryPredictor;
		[SerializeField] private NpcManager npcManager;
		[SerializeField] private FieldCollider fieldCollider;
		[SerializeField] private DurationProgressBar progressBar;

		[SerializeField] private Dice.Dice dicePrefab;
		[SerializeField] private Npc.Npc npcPrefab;
		[SerializeField] private BaseState npcFirstState;
		[SerializeField] private EffectHandler explosionEffectPrefab;

		private readonly Guid id = Guid.NewGuid();

		#endregion



		#region Properties

		public TeamInfo TeamInfo => new TeamInfo()
		{
			id = id,
			npcManager = npcManager
		};

		#endregion



		#region Methods

		public override void InstallBindings()
		{
			Container.DeclareSignal<DiceLandedSignal>();

			Container.Bind<Vector3>().FromInstance(cannon.transform.position).AsSingle();

			Container.Bind<int>().FromInstance(10).AsSingle();
			Container.Bind<Guid>().FromInstance(id).AsSingle();

			Container.BindFactory<Dice.Dice, Factory>().FromComponentInNewPrefab(dicePrefab).AsSingle();
			Container.Bind<NpcPoolFactory>().AsSingle();

			Container.Bind<Npc.Npc>().FromInstance(npcPrefab).AsSingle();
			Container.BindInterfacesAndSelfTo<AiStateMachine>().FromNew().AsTransient().Lazy();
			Container.Bind<BaseState>().To(npcFirstState.GetType()).FromScriptableObject(npcFirstState).AsSingle();
			Container.Bind<EffectHandler>().FromInstance(explosionEffectPrefab).AsSingle();

			Container.Bind<Cannon>().FromInstance(cannon).AsSingle();
			Container.Bind<TrajectoryPredictor>().FromInstance(trajectoryPredictor).AsSingle();
			Container.BindInterfacesAndSelfTo<NpcManager>().FromInstance(npcManager).AsSingle();
			Container.Bind<FieldCollider>().FromInstance(fieldCollider).AsSingle();
			Container.Bind<DurationProgressBar>().FromInstance(progressBar).AsSingle();

			Container.BindInterfacesAndSelfTo(controller.GetType()).FromScriptableObject(controller).AsSingle();

			Container.BindSignal<DiceLandedSignal>()
				.ToMethod<NpcManager>((manager, signal) => manager.CreateGroup(signal.amount, signal.position)).FromResolve();

			Container.BindSignal<GameEndSignal>().ToMethod(controller.Stop);
			Container.BindSignal<GameEndSignal>().ToMethod(npcManager.Stop);
			Container.BindSignal<StartGameSignal>().ToMethod(controller.Start);
			Container.BindSignal<StartGameSignal>().ToMethod(npcManager.StartGame);
		}
		
		#endregion
	}
}
