using System;
using System.Collections.Generic;
using Npc;
using UnityEngine;


namespace Cannon
{
	public class TeamInfo
	{
		public Guid id;
		public NpcManager npcManager;
	}
}
