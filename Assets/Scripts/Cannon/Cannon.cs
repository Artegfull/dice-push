using System;
using System.Collections;
using DG.Tweening;
using Dice;
using UnityEngine;
using Utility;
using Zenject;


namespace Cannon
{
    public class Cannon : MonoBehaviour
    {
        #region Fields

        [SerializeField] private float shootingForce;
        [SerializeField] private float diceSpawnOffset;
        [SerializeField] private MinMaxVector minMaxRotation;
        [SerializeField] private Transform gun;
        [SerializeField] private Transform transformForEffectPosition;

        #endregion



        #region Injection

        [Inject] private Factory diceFactory;
        [Inject] private TrajectoryPredictor trajectoryPredictor;
        [Inject] private EffectHandler effectPrefab;
        

        #endregion



        #region Properties

        private Vector3 DiceSpawnPosition => gun.position + gun.forward * diceSpawnOffset;


        private Vector3 ShootingDirection => gun.forward;

        #endregion



        #region Unity lifecycle

        private void OnDestroy()
        {
            DOTween.Kill(this);
        }

        #endregion



        #region Methods

        public void RotateFromScreenDelta(Vector2 screenDelta)
        {
            screenDelta *= Time.deltaTime;
        
            transform.Rotate(0, screenDelta.x, 0);
            transform.rotation = minMaxRotation.GetClampedQuaternion(transform.rotation);
        
            gun.Rotate(-screenDelta.y, 0, 0);
            gun.rotation = minMaxRotation.GetClampedQuaternion(gun.rotation);
        
            UpdateTrajectory();
        }


        public void AimInDirection(Vector3 direction, float duration)
        {
            Vector3 horizontalProjection = Vector3.ProjectOnPlane(direction, Vector3.up);
            transform.DOLookAt(horizontalProjection, duration).SetId(this);

            Vector3 verticalProjection = Vector3.ProjectOnPlane(direction, transform.right);
            gun.DOLookAt(verticalProjection, duration).SetId(this);
        
            UpdateTrajectoryEveryFrame(duration);
        }


        public void Shoot()
        {
            Dice.Dice dice = diceFactory.Create();
            dice.transform.position = DiceSpawnPosition;

            Vector3 force = ShootingDirection * shootingForce;

            dice.Shoot(force);
            ShowEffect();
        
            trajectoryPredictor.HideAfterLanding(force);
        }


        public void Stop()
        {
            trajectoryPredictor.HideTrajectory();
        }


        private void UpdateTrajectory()
        {
            trajectoryPredictor.UpdateTrajectory(shootingForce * ShootingDirection, DiceSpawnPosition);
        }


        private void ShowEffect()
        {
            EffectHandler effectHandler = Instantiate(effectPrefab, transformForEffectPosition);
            effectHandler.PlayEffect();
        }


        private void UpdateTrajectoryEveryFrame(float duration)
        {
            StartCoroutine(UpdateTrajectoryCoroutine(duration));
        }


        private IEnumerator UpdateTrajectoryCoroutine(float duration)
        {
            float timePassed = 0f;

            while (timePassed < duration)
            {
                timePassed += Time.deltaTime;
            
                UpdateTrajectory();

                yield return new WaitForEndOfFrame();
            }
        }

        #endregion
    }
}
