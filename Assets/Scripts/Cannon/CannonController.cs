using UnityEngine;
using Zenject;


namespace Cannon
{
	public abstract class CannonController : ScriptableObject
	{
		[Inject] protected Cannon cannon;
		
		
		public abstract void Stop();


		public abstract void Start();
	}
}
