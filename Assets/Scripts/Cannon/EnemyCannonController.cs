using System;
using DG.Tweening;
using UnityEngine;
using Utility;
using Zenject;


namespace Cannon
{
    [CreateAssetMenu(fileName = "EnemyCannonController", menuName = "Cannon/EnemyController")]
    public class EnemyCannonController : CannonController, IInitializable
    {
        #region Fields

        [SerializeField] private MinMaxValue minMaxCooldown;
        [SerializeField] private MinMaxValue minMaxShootAfterAimDelay;
        [SerializeField] private MinMaxVector minMaxShootDirection;

        #endregion



        #region Injection

        [Inject] private DurationProgressBar progressBar;

        #endregion



        #region Unity lifecycle

        private void OnDisable()
        {
            DOTween.Kill(this);
        }

        #endregion
        
        

        #region Methods
        
        public override void Start()
        {
            IterateShootCycle();
        }
        

        public override void Stop()
        {
            DOTween.Kill(this);
            cannon.Stop();   
        }


        private void IterateShootCycle()
        {
            float cooldown = minMaxCooldown.GetRandom();

            DOVirtual.DelayedCall(cooldown, IterateShootCycle).SetId(this);
            Shoot(cooldown);
        }


        private void Shoot(float cooldown)
        {
            Vector3 shootDirection = minMaxShootDirection.GetRandom();

            float aimDuration = minMaxShootAfterAimDelay.GetRandom();
        
            cannon.AimInDirection(shootDirection, aimDuration);
            DOVirtual.DelayedCall(aimDuration, () =>
            {
                progressBar.Fill(cooldown - aimDuration);
                cannon.Shoot();
            }).SetId(this);
        }

        #endregion



        public void Initialize()
        {
            
        }
    }
}
