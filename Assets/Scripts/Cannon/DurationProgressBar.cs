using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


namespace Cannon
{
	public class DurationProgressBar : MonoBehaviour
	{
		[SerializeField] private Slider slider;


		public void Fill(float duration)
		{
			Reset();
			
			DOTween.To(() => slider.value, (val) => slider.value = val, 1, duration).SetId(this);
		}


		private void Reset()
		{
			slider.value = 0;
		}


		private void OnDestroy()
		{
			DOTween.Kill(this);
		}
	}
}
