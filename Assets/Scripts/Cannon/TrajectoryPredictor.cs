using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


namespace Cannon
{
    public class TrajectoryPredictor : MonoBehaviour
    {
        #region Fields

        private const int EXTRA_SEGMENTS_COUNT = 2;
        
        [SerializeField] private LineRenderer lineRenderer;
        [SerializeField] [Range(3, 30)] private int lineSegmentsCount;

        private List<Vector3> linePoints = new List<Vector3>();

        #endregion



        #region Properties

        private float FlightDuration(Vector3 force) => 2 * force.y * Time.fixedDeltaTime / -Physics.gravity.y;

        #endregion
        
        
        #region Unity lifecycle

        private void OnDestroy()
        {
            DOTween.Kill(this);
        }

        #endregion




        #region Methods

        public void UpdateTrajectory(Vector3 force, Vector3 startingPoint)
        {
            Vector3 velocity = force * Time.fixedDeltaTime;

            float timeStep = FlightDuration(force) / lineSegmentsCount;
        
            linePoints.Clear();

            for (int i = 0; i < lineSegmentsCount + EXTRA_SEGMENTS_COUNT; i++)
            {
                float stepTimePassed = timeStep * i;

                Vector3 movement = new Vector3(
                    velocity.x * stepTimePassed,
                    velocity.y * stepTimePassed + 0.5f * Physics.gravity.y * stepTimePassed * stepTimePassed,
                    velocity.z * stepTimePassed);
            
                linePoints.Add(movement + startingPoint);
            }

            lineRenderer.positionCount = linePoints.Count;
            lineRenderer.SetPositions(linePoints.ToArray());
        }


        public void HideAfterLanding(Vector3 force)
        {
            DOVirtual.DelayedCall(FlightDuration(force), HideTrajectory).SetId(this);
        }


        public void HideTrajectory()
        {
            lineRenderer.positionCount = 0;
        }

        #endregion
    }
}
