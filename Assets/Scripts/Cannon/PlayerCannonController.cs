using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using Utility;
using Zenject;


namespace Cannon
{
    [CreateAssetMenu(fileName = "PlayerCannonController", menuName = "Cannon/PlayerCannonController")]
    public class PlayerCannonController : CannonController, ITickable, IInitializable, IDragHandler, IEndDragHandler
    {
        #region Fields

        [SerializeField] private float sensitivity;
        [SerializeField] private MinMaxValue minMaxCooldown;
        
        private Vector3 previousMousePosition;
        private bool isLocked = true;

        #endregion



        #region Injection

        [Inject] private DurationProgressBar progressBar;

        #endregion
        
        
        
        #region Methods

        public override void Start()
        {
            isLocked = false;
        }
        
        
        public override void Stop()
        {
            DOTween.Kill(this);
            isLocked = true;
            cannon.Stop();
        }


        private void UpdateMousePosition()
        {
            previousMousePosition = Input.mousePosition;
        }


        private void SetupCooldown()
        {
            isLocked = true;

            float cooldown = minMaxCooldown.GetRandom();
            
            progressBar.Fill(cooldown);
            DOVirtual.DelayedCall(cooldown, () => isLocked = false).SetId(this);
        }

        #endregion



        #region ITickable

        public void Tick()
        {
            if (isLocked)
            {
                return;
            }
            
            if (Input.GetMouseButton(0))
            {
                Vector2 delta = Input.mousePosition - previousMousePosition;
                delta *= sensitivity;
            
                cannon.RotateFromScreenDelta(delta);
            }

            if (Input.GetMouseButtonUp(0))
            {
                cannon.Shoot();

                SetupCooldown();
            }

            UpdateMousePosition();
        }

        #endregion



        #region IInitializable

        public void Initialize()
        {
            isLocked = true;
        }
        
        #endregion
        
        

        #region IDragHandler

        public void OnDrag(PointerEventData eventData)
        {
            if (isLocked)
            {
                return;
            }

            Vector2 delta = eventData.delta * sensitivity;

            cannon.RotateFromScreenDelta(delta);
        }

        #endregion



        #region IEndDragHandler

        public void OnEndDrag(PointerEventData eventData)
        {
            if (isLocked)
            {
                return;
            }
            
            cannon.Shoot();

            SetupCooldown();
        }

        #endregion
    }
}
