using System;
using UnityEngine;


namespace Npc
{
	public interface IMatchController
	{
		Transform GetClosestEnemyTransform(Vector3 position, Guid teamId);
	}
}
