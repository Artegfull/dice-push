using DG.Tweening;
using UnityEngine;


namespace Npc.NpcStates
{
	[CreateAssetMenu(fileName = "AttackAiState", menuName = "Npc/States/Attack", order = 0)]
	public class AttackState : BaseState
	{
		#region Fields

		[SerializeField] private float findEnemyDurationStep;
		
		private Transform followedTransform;
		private bool isFindingReloading;
		
		#endregion



		#region Methods

		public override void Enter(AiStateMachine aiStateMachine)
		{
			base.Enter(aiStateMachine);

			isFindingReloading = false;
			TryFindTarget();
		}


		private void TryFindTarget()
		{
			if (isFindingReloading)
			{
				return;
			}
			
			followedTransform = stateMachine.GetClosestEnemyTransform();
			isFindingReloading = true;

			DOVirtual.DelayedCall(findEnemyDurationStep,() => isFindingReloading = false).SetId(this);
		}


		public override void Tick()
		{
			if (followedTransform == null || !followedTransform.gameObject.activeSelf)
			{
				TryFindTarget();
			}
			else
			{
				stateMachine.Npc.MoveTowards(followedTransform.position);
			}
		}


		public override void Exit()
		{
			isFindingReloading = false;
		}

		#endregion
	}
}
