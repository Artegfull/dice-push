using UnityEngine;


namespace Npc.NpcStates
{
	public abstract class BaseState : ScriptableObject
	{
		#region Fields

		protected AiStateMachine stateMachine;

		#endregion



		#region Methods

		public virtual void Enter(AiStateMachine aiStateMachine)
		{
			stateMachine = aiStateMachine;
		}


		public virtual void Tick() { }


		public virtual void Exit() { }
		

		public virtual void OnTriggerEnter(Collider collider) { }
		
		
		public virtual void Destroy() { }

		#endregion
	}
}
