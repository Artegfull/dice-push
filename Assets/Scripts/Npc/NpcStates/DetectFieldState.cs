using Cannon;
using UnityEngine;


namespace Npc.NpcStates
{
	[CreateAssetMenu(fileName = "DetectSideAiState", menuName = "Npc/States/DetectSide")]
	public class DetectFieldState : BaseState
	{
		#region Fields

		[SerializeField] private BaseState nextStateOnEnemyField;
		[SerializeField] private BaseState nextStateOnTeamsField;

		#endregion



		#region Methods

		public override void OnTriggerEnter(Collider collider)
		{
			FieldCollider fieldCollider = collider.GetComponent<FieldCollider>();

			if (fieldCollider != null)
			{
				BaseState nextState = (fieldCollider.TeamId != stateMachine.TeamId) ? 
					nextStateOnEnemyField : nextStateOnTeamsField;
				
				stateMachine.TransitToState(nextState);
			}
		}

		#endregion
	}
}
