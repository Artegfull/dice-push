using DG.Tweening;
using UnityEngine;


namespace Npc.NpcStates
{
	[CreateAssetMenu(fileName = "PushAiState", menuName = "Npc/States/Push", order = 0)]
	public class PushState : BaseState
	{
		#region Fields

		[SerializeField] private float findingPlaceReload;
		
		private Vector3 movementDirection;
		private PushPlace pushPlace;
		
		private bool isLocked;
		private bool isReloadingFindingPlace;

		#endregion



		#region Methods

		public override void Enter(AiStateMachine aiStateMachine)
		{
			base.Enter(aiStateMachine);

			Reset();
			TryUpdatePushPlace();
		}


		public override void Exit()
		{
			Reset();

			DOTween.Kill(this);
		}


		public override void Tick()
		{
			if (isLocked || pushPlace == null)
			{
				return;
			}

			stateMachine.Npc.MoveTowards(pushPlace.Position);
			
			if (pushPlace.TryAttach(stateMachine.Npc.transform))
			{
				stateMachine.Npc.FixInPlace();
				isLocked = true;
			}
		}


		public override void Destroy()
		{
			if (isLocked && stateMachine.Npc != null)
			{
				stateMachine.Npc.ShowPopup();
			}
			
			pushPlace?.Free();
		}


		private void TryUpdatePushPlace()
		{
			if (isReloadingFindingPlace)
			{
				return;
			}
			
			pushPlace =
				stateMachine.Barrier.GetClosestEmptyRelativePlace(stateMachine.Npc.transform.position);
			isReloadingFindingPlace = true;

			DOVirtual.DelayedCall(findingPlaceReload, () => isReloadingFindingPlace = false).SetId(this);
		}


		private void Reset()
		{
			isLocked = false;
			isReloadingFindingPlace = false;
		}

		#endregion
	}
}
