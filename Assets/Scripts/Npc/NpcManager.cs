using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Utility;
using Zenject;


namespace Npc
{
    public class NpcManager : MonoBehaviour
    {
        #region Fields

        [SerializeField] private int amountOfNpcAddedOnStart;
        [SerializeField] private Vector3 localPositionOfStartNpcs;
        
        private List<Npc> npcsOnScene = new List<Npc>();
        
        private bool isLocked;

        #endregion



        #region Injection

        [Inject] private NpcPoolFactory npcFactory;
        [Inject] private IBarrier barrier;

        #endregion



        #region Methods

        public void StartGame()
        {
            CreateGroup(amountOfNpcAddedOnStart, transform.TransformPoint(localPositionOfStartNpcs));
        }
        

        public void CreateGroup(int amountOfNpcs, Vector3 groupPosition)
        {
            if (isLocked)
            {
                return;
            }
            
            float radius = amountOfNpcs > 1 ? 1f : 0f;
            float angleStep = 360f / amountOfNpcs;
            
            for (int i = 0; i < amountOfNpcs; i++)
            {
                Vector3 spawnPosition = GetPositionOnCircle(groupPosition, radius, angleStep * i);
                
                AddNpc(spawnPosition);
            }
        }


        public void Free(Npc npc)
        {
            npcsOnScene.Remove(npc);
            npcFactory.Free(npc);
        }


        public Transform GetClosestNpcTransform(Vector3 position)
        {
            if (npcsOnScene.Count == 0)
            {
                return null;
            }

            Transform result = npcsOnScene[0].transform;
            float minSqrDistance = (result.position - position).sqrMagnitude;

            for (int i = 1; i < npcsOnScene.Count; i++)
            {
                Transform currentTransform = npcsOnScene[i].transform;
                Vector3 currentPosition = currentTransform.position;
                float currentSqrDistance = (currentPosition - position).sqrMagnitude;
                
                if (currentSqrDistance < minSqrDistance && barrier.AreOnTheSameSide(transform.position, currentPosition))
                {
                    result = currentTransform;
                    minSqrDistance = currentSqrDistance;
                }
            }

            return result;
        }


        public void Stop()
        {
            foreach (Npc npc in npcsOnScene)
            {
                npc.Stop();
            }
        }


        private Vector3 GetPositionOnCircle(Vector3 center, float radius, float angle)
        {
            float radians = Mathf.Deg2Rad * angle;

            float xOffset = Mathf.Cos(radians);
            float zOffset = Mathf.Sin(radians);

            return center + new Vector3(xOffset, 0f, zOffset) * radius;
        }


        private void AddNpc(Vector3 position)
        {
            Npc npc = npcFactory.Create();
            
            npc.transform.SetParent(transform);
            npc.transform.position = position;
            
            npcsOnScene.Add(npc);
        }

        #endregion
    }
}
