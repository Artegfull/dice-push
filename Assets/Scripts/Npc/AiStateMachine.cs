using System;
using Npc.NpcStates;
using UnityEngine;
using Object = UnityEngine.Object;


namespace Npc
{
	public class AiStateMachine
	{
		#region Fields

		private IMatchController matchController;
		private IBarrier barrier;
		private Npc npc;
		
		private Guid teamId;
		private Vector3 cannonPosition;
		private BaseState startState;
		
		private BaseState currentState;

		private bool isEnabled = true;

		#endregion
		
		

		#region Properties

		public Npc Npc => npc;


		public Guid TeamId => teamId;


		public Vector3 CannonPosition => cannonPosition;


		public IBarrier Barrier => barrier;

		#endregion


		
		#region Inject

		public AiStateMachine(BaseState startState, Guid id, Vector3 position, IMatchController matchController, IBarrier barrier)
		{
			teamId = id;
			cannonPosition = position;
			this.matchController = matchController;
			this.barrier = barrier;
			this.startState = startState;

			currentState = Object.Instantiate(startState);
			
			currentState.Enter(this);
		}

		#endregion



		#region Methods

		public Transform GetClosestEnemyTransform()
		{
			return matchController.GetClosestEnemyTransform(npc.transform.position, teamId);
		}


		public void Start(Npc npc)
		{
			this.npc = npc;
		}
		

		public void Update()
		{
			if (!isEnabled)
			{
				return;
			}
			
			currentState.Tick();
		}


		public void OnTriggerEnter(Collider collider)
		{
			if (!isEnabled)
			{
				return;
			}
			
			currentState.OnTriggerEnter(collider);
		}


		public void Destroy()
		{
			if (!isEnabled)
			{
				return;
			}
			
			currentState.Destroy();
		}


		public void TransitToState(BaseState nextState)
		{
			currentState.Exit();

			currentState = Object.Instantiate(nextState);

			currentState.Enter(this);
		}


		public void Toggle(bool isEnabled)
		{
			this.isEnabled = isEnabled;
		}


		public void Reset()
		{
			TransitToState(Object.Instantiate(startState));
		}

		#endregion
	}
}
