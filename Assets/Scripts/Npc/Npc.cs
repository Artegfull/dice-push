using System;
using DG.Tweening;
using UnityEngine;
using Utility;
using Zenject;


namespace Npc
{
	public class Npc : MonoBehaviour
	{
		#region Fields

		private const float OUT_OF_BOUNDS_Y = -5;

		[SerializeField] private new Rigidbody rigidbody;
		[SerializeField] private float speed;
		[SerializeField] private Popup popupPrefab;
		
		private bool isLocked;

		#endregion



		#region Injection

		[Inject] private Guid teamId;
		[Inject] private AiStateMachine aiStateMachine;
		[Inject] private NpcManager npcManager;

		#endregion



		#region Unity lifecycle

		private void Awake()
		{
			aiStateMachine.Start(this);
		}
		
		
		public void OnEnable()
		{
			aiStateMachine.Toggle(true);
		}


		private void Update()
		{
			aiStateMachine.Update();

			TryDestroyOutOfBounds();
		}
		
		
		private void OnTriggerEnter(Collider other)
		{
			aiStateMachine.OnTriggerEnter(other);
		}


		private void OnCollisionEnter(Collision other)
		{
			Npc otherNpc = other.gameObject.GetComponent<Npc>();

			if (otherNpc != null && otherNpc.teamId != teamId)
			{
				Destroy();
			}

			Dice.Dice dice = other.gameObject.GetComponent<Dice.Dice>();

			if (dice != null && !dice.IsOnThisTeam(teamId))
			{
				Destroy();
			}
		}
		
		#endregion



		#region Methods

		public void FixInPlace()
		{
			rigidbody.isKinematic = true;
		}


		public void MoveTowards(Vector3 position)
		{
			if (isLocked)
			{
				return;
			}
			
			Vector3 direction = (position - transform.position).normalized;
			
			MoveInDirection(direction);
		}


		public void Stop()
		{
			isLocked = true;
		}


		public void ShowPopup()
		{
			Instantiate(popupPrefab, transform.position, Quaternion.identity);
		}


		private void Reset()
		{
			aiStateMachine.Toggle(false);
			aiStateMachine.Reset();
			
			rigidbody.isKinematic = false;
		}


		private void MoveInDirection(Vector3 direction)
		{
			Vector3 movement = direction * (Time.deltaTime * speed);

			rigidbody.MovePosition(transform.position + movement);
		}


		private void TryDestroyOutOfBounds()
		{
			if (transform.position.y < OUT_OF_BOUNDS_Y)
			{
				Destroy();
			}
		}
		

		private void Destroy()
		{
			aiStateMachine.Destroy();
			Reset();

			npcManager.Free(this);
		}

		#endregion
	}
}

