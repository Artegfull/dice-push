using System.Collections.Generic;
using UnityEngine;
using Utility;
using Zenject;


namespace Npc
{
	public class NpcPoolFactory : IFactory<Npc>
	{
		#region Fields

		private readonly Queue<Npc> instances;
		private readonly DiContainer container;
		private readonly Npc npcPrefab;

		#endregion



		#region Injection

		public NpcPoolFactory(DiContainer container, Npc npcPrefab, int initialSpawnedCount)
		{
			instances = new Queue<Npc>();
			this.container = container;
			this.npcPrefab = npcPrefab;
			
			PopulatePool(initialSpawnedCount);
		}

		#endregion



		#region Methods
		
		public void Free(Npc instance)
		{
			ToggleVisibility(instance, false);

			instances.Enqueue(instance);
		}
		
		
		protected virtual Npc Instantiate()
		{
			Npc instance = container.InstantiatePrefabForComponent<Npc>(npcPrefab);
			ToggleVisibility(instance, false);

			return instance;
		}


		private void PopulatePool(int amount)
		{
			for (int i = 0; i < amount; i++)
			{
				instances.Enqueue(Instantiate());
			}
		}


		private void ToggleVisibility(Npc instance, bool isEnabled)
		{
			instance.gameObject.SetActive(isEnabled);
		}

		#endregion



		#region IFactory<Npc>

		public virtual Npc Create()
		{
			Npc instance = instances.Count > 0 ? instances.Dequeue() : Instantiate();
			ToggleVisibility(instance, true);

			return instance;
		}

		#endregion
	}
}
