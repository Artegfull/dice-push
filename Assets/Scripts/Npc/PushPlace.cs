using System;
using UnityEngine;


namespace Npc
{
	[Serializable]
	public class PushPlace
	{
		#region Fields

		private const float EPSILON = 0.05f;

		private readonly IBarrier barrier;
		private readonly Vector3 localPosition;
		
		private bool isReserved;
		private bool isTaken;

		#endregion



		#region Properties

		public Vector3 Position => barrier.GetPositionFromPlace(this);


		public Vector3 LocalPosition => localPosition;


		public bool IsTaken => isTaken;


		public bool IsReserved
		{
			get => isReserved;
			set => isReserved = value;
		}
		

		#endregion



		#region Methods
		
		public PushPlace(IBarrier barrier, Vector3 localPosition)
		{
			this.localPosition = localPosition;
			this.barrier = barrier;
		}
		

		public void Free()
		{
			barrier.FreePusher(this);
			
			isReserved = false;
			isTaken = false;
		}
		

		public bool TryAttach(Transform transform)
		{
			float sqrMagnitude = (transform.position - barrier.GetPositionFromPlace(this)).sqrMagnitude;

			if (sqrMagnitude < EPSILON)
			{
				isTaken = true;
				Attach(transform);
				barrier.AddPusher(this);
				return true;
			}

			return false;
		}


		private void Attach(Transform attachedTransform)
		{
			barrier.Attach(attachedTransform);
			attachedTransform.position = Position;
		}

		#endregion
	}
}
