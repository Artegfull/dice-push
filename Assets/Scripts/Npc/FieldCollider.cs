using System;
using Npc;
using UnityEngine;
using Zenject;


namespace Cannon
{
	public class FieldCollider : MonoBehaviour
	{
		#region Injection

		[Inject] private Guid teamId;
		[Inject] private IBarrier barrier;

		#endregion



		#region Properties

		public Guid TeamId => teamId;

		#endregion



		#region Unity lifecycle

		private void Start()
		{
			barrier.Attach(transform);
		}

		#endregion
	}
}
