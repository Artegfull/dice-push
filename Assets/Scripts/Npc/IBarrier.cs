using UnityEngine;


namespace Npc
{
	public interface IBarrier
	{
		PushPlace GetClosestEmptyRelativePlace(Vector3 position);


		Vector3 GetPositionFromPlace(PushPlace pushPlace);


		bool AreOnTheSameSide(Vector3 first, Vector3 second);


		void Attach(Transform transform);


		void FreePusher(PushPlace pushPlace);


		void AddPusher(PushPlace pushPlace);
	}
}
