using UnityEngine;


namespace Dice
{
	public class DiceLandedSignal
	{
		public int amount;
		public Vector3 position;
	}
}
