using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Utility;
using Zenject;
using Random = UnityEngine.Random;


namespace Dice
{
    [RequireComponent(typeof(Rigidbody))]
    public class Dice : MonoBehaviour
    {
        #region Fields

        private const float OUT_OF_BOUNDS_Y = -5;
        
        [SerializeField] private float enableColliderDelay;
        [SerializeField] private float checkVelocityStep;
        [SerializeField] private float maxLifetime;
        [SerializeField] private float torgueMultiplier;

        [SerializeField] private new Rigidbody rigidbody;
        [SerializeField] private new Collider collider;
        [SerializeField] private Transform[] sides;
        [SerializeField] private EffectHandler effectHandler;

        #endregion



        #region Injection

        [Inject] private SignalBus signalBus;
        [Inject] private Guid teamId;

        #endregion



        #region Properties

        public bool IsOnThisTeam(Guid id) => id == teamId;

        #endregion
        
        

        #region Unity lifecycle

        private void Update()
        {
            if (transform.position.y < OUT_OF_BOUNDS_Y)
            {
                Destroy(gameObject);
            }
        }
        
        
        private void OnDestroy()
        {
            DOTween.Kill(this);
        }

        #endregion



        #region Methods

        public void Shoot(Vector3 force)
        {
            rigidbody.AddForce(force);

            Vector3 randomVector = new Vector3(
                Random.Range(-1, 1),
                Random.Range(-1, 1),
                Random.Range(-1, 1));
            
            rigidbody.AddTorque(randomVector * torgueMultiplier);
        
            SetupColliderDelayedEnable();
            SetupVelocityCheck();
            
            signalBus.Fire<DiceShotSignal>();
        }


        
        private void SetupColliderDelayedEnable()
        {
            collider.enabled = false;

            DOVirtual.DelayedCall(enableColliderDelay, () => collider.enabled = true).SetId(this);
        }


        private void SetupVelocityCheck()
        {
            DOVirtual.DelayedCall(maxLifetime, RegisterDiceLand).SetId(this);
            
            StartCoroutine(CheckVelocityCoroutine());
        }


        private IEnumerator CheckVelocityCoroutine()
        {
            Vector3 velocityWithoutZ;
            
            do
            {
                yield return new WaitForSeconds(checkVelocityStep);

                velocityWithoutZ = rigidbody.velocity;
                velocityWithoutZ.z = 0;
            } while (!Mathf.Approximately(velocityWithoutZ.sqrMagnitude, 0f));
        
            RegisterDiceLand();
        }


        private void RegisterDiceLand()
        {
            int diceScore = GetScore();
            
            signalBus.Fire(new DiceLandedSignal()
            {
                amount = diceScore,
                position = transform.position
            });

            ShowEffect();
            
            Destroy(gameObject);
        }


        private int GetScore()
        {
            float maxY = float.MinValue;
            int score = 0;
            
            for (int i = 0; i < sides.Length; i++)
            {
                float currentY = sides[i].transform.position.y;
                
                if (currentY > maxY)
                {
                    score = i;
                    maxY = currentY;
                }
            }

            return score + 1;
        }


        private void ShowEffect()
        {
            EffectHandler effect = Instantiate(effectHandler);
            effect.transform.position = transform.position;
            
            effect.PlayEffect();
        }

        #endregion
    }
}
