using System;
using System.Collections.Generic;
using System.Diagnostics;
using Cannon;
using DG.Tweening;
using Npc;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;


namespace Core
{
    public class MatchController : MonoBehaviour, IMatchController
    {
        #region Fields

        [SerializeField] private float restartGameDelay = 3f;

        private Dictionary<Guid, TeamInfo> idToInfo = new Dictionary<Guid, TeamInfo>();

        #endregion



        #region Injection

        [Inject] private List<CannonInstaller> installers;
        [Inject] private SignalBus signalBus;

        #endregion



        #region Unity lifecycle

        private void Start()
        {
            PopulateIdToInfo();
        }


        [Conditional("UNITY_EDITOR")]
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                signalBus.Fire<GameEndSignal>();
                ReloadCurrentScene();
            }
        }

        #endregion



        #region Methods

        public void RestartGameDelayed()
        {
            DOVirtual.DelayedCall(restartGameDelay, ReloadCurrentScene);
        }


        public Transform GetClosestEnemyTransform(Vector3 position, Guid teamId)
        {
            foreach (KeyValuePair<Guid,TeamInfo> idToInfoPair in idToInfo)
            {
                if (idToInfoPair.Key == teamId)
                {
                    continue;
                }

                return idToInfoPair.Value.npcManager.GetClosestNpcTransform(position);
            }

            return null;
        }


        private void ReloadCurrentScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }


        private void PopulateIdToInfo()
        {
            foreach (CannonInstaller installer in installers)
            {
                TeamInfo info = installer.TeamInfo;

                idToInfo.Add(info.id, info);
            }
        }

        #endregion
    }
}
