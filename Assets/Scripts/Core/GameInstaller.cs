using Ui.Windows;
using Cannon;
using Dice;
using Npc;
using Ui;
using UnityEngine;
using Utility;
using Zenject;


namespace Core
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        #region Fields

        [SerializeField] private MatchController matchController;
        [SerializeField] private Barrier barrier;
        [SerializeField] private CameraController cameraController;
        [SerializeField] private WindowManager windowManager;
        [SerializeField] private StartMenuWindow startMenuWindowPrefab;
        [SerializeField] private EndMenuWindow endMenuWindowPrefab;

        #endregion



        #region Methods

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<GameEndSignal>();
            Container.DeclareSignal<DiceShotSignal>();
            Container.DeclareSignal<StartGameSignal>();
            Container.DeclareSignal<RestartGameSignal>();

            Container.Bind<IBarrier>().To<Barrier>().FromInstance(barrier).AsSingle();
            Container.Bind<IMatchController>().To<MatchController>().FromInstance(matchController).AsSingle();
            Container.Bind<CameraController>().AsSingle();
            Container.Bind<WindowManager>().FromInstance(windowManager).AsSingle();
            
            Container.Bind<StartMenuWindow>().FromInstance(startMenuWindowPrefab).AsSingle();
            Container.Bind<EndMenuWindow>().FromInstance(endMenuWindowPrefab).AsSingle();

            Container.BindSignal<GameEndSignal>().ToMethod<WindowManager>((manager, signal) =>
            {
                EndMenuWindow window = windowManager.ShowWindow(endMenuWindowPrefab) as EndMenuWindow;
                window.SetWinner(signal.isPlayerWon); 
            }).FromResolve();
            
            Container.BindSignal<RestartGameSignal>().ToMethod(matchController.RestartGameDelayed);
            Container.BindSignal<DiceShotSignal>().ToMethod(cameraController.Shake);
        }


        public override void Start()
        {
            base.Start();
            
            windowManager.ShowWindow(startMenuWindowPrefab);
        }

        #endregion
    }
}
