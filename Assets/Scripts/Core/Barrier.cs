using System.Collections.Generic;
using System.Diagnostics;
using Cannon;
using Npc;
using UnityEngine;
using Zenject;


namespace Core
{
	public class Barrier : MonoBehaviour, IBarrier
	{
		#region Fields

		[SerializeField] private float rowWidth;
		[SerializeField] private float stepInRow;
		[SerializeField] private float rowZOffset;
		[SerializeField] private int maxAmountFoRows;
		[SerializeField] private float movementMultiplier;
		[SerializeField] private float distanceForGameEnd;
		
		private List<List<PushPlace>> playerPlaces = new List<List<PushPlace>>();
		private List<List<PushPlace>> enemyPlaces = new List<List<PushPlace>>();
		private int amountOfPlacesInRow;
		private Vector3 initialPosition;
		
		private int gameBalance;
		private bool isLocked;

		#endregion



		#region Injection

		[Inject] private SignalBus signalBus;

		#endregion



		#region Properties

		private bool IsPlaceForPlayer(PushPlace pushPlace) => pushPlace.Position.z < transform.position.z;

		#endregion


		
		#region Unity lifecycle

		private void Start()
		{
			amountOfPlacesInRow = (int) (rowWidth / stepInRow);

			initialPosition = transform.position;

			PopulateList(playerPlaces, -rowZOffset);
			PopulateList(enemyPlaces, rowZOffset);
		}


		private void Update()
		{
			if (isLocked)
			{
				return;
			}	
			
			MoveBarrier();

			TryEndGame();
		}

		#endregion



		#region Methods

		private void MoveBarrier()
		{
			Vector3 direction = Vector3.forward * (gameBalance * movementMultiplier * Time.deltaTime);

			transform.Translate(direction);
		}


		private void TryEndGame()
		{
			float distance = transform.position.z - initialPosition.z;

			if (Mathf.Abs(distance) > distanceForGameEnd)
			{
				EndGame(distance);
			}
		}


		private void EndGame(float distance)
		{
			signalBus.Fire(new GameEndSignal()
			{
				isPlayerWon = distance > 0
			});

			FreeAll();

			isLocked = true;
			gameBalance = 0;
		}


		private void FreeAll()
		{
			FreeAllPlaces(playerPlaces);
			FreeAllPlaces(enemyPlaces);
		}


		private void FreeAllPlaces(List<List<PushPlace>> places)
		{
			foreach (List<PushPlace> list in places)	
			{
				foreach (PushPlace place in list)
				{
					FreePusher(place);
				}
			}
		}


		private void PopulateList(List<List<PushPlace>> places, float zOffset)
		{
			for (int i = 0; i < maxAmountFoRows; i++)
			{
				places.Add(new List<PushPlace>());

				for (int j = 0; j < amountOfPlacesInRow; j++)
				{
					places[i].Add(new PushPlace(this,
						new Vector3(-rowWidth / 2 + stepInRow * (j + 0.5f), 0, zOffset * (i + 1))));
				}
			}
		}
		
		
		private bool IsRowFull(List<List<PushPlace>> places, int rowNumber)
		{
			for (int i = 0; i < amountOfPlacesInRow; i++)
			{
				if (!places[rowNumber][i].IsReserved)
				{
					return false;
				}
			}

			return true;
		}


		private PushPlace GetClosestPlaceInRow(List<List<PushPlace>> places, int rowNumber, Vector3 position)
		{
			PushPlace place = null;
			float minSqrMagnitude = float.MaxValue;
			
			for (int i = 0; i < amountOfPlacesInRow; i++)
			{
				PushPlace currentPlace = places[rowNumber][i];

				if (currentPlace.IsReserved)
				{
					continue;
				}
				
				Vector3 currentPosition = places[rowNumber][i].LocalPosition;
				float currentSqrMagnitude = (currentPosition - position).sqrMagnitude;

				if (currentSqrMagnitude < minSqrMagnitude)
				{
					place = currentPlace;
					minSqrMagnitude = currentSqrMagnitude;
				}
			}
			
			return place;
		}
		
		
		private void TakePlace(PushPlace pushPlace)
		{
			pushPlace.IsReserved = true;
		}

		#endregion



		#region IBarrier
		
		public PushPlace GetClosestEmptyRelativePlace(Vector3 position)
		{
			bool isPlayer = position.z < transform.position.z;
			List<List<PushPlace>> places = isPlayer ? playerPlaces : enemyPlaces;
			
			for (int i = 0; i < maxAmountFoRows; i++)
			{
				if (IsRowFull(places, i))
				{
					continue;
				}
				
				PushPlace resultPlace = GetClosestPlaceInRow(places, i, position);
				TakePlace(resultPlace);
				
				return resultPlace;
			}

			return null;
		}

		public Vector3 GetPositionFromPlace(PushPlace pushPlace)
		{
			return transform.TransformPoint(pushPlace.LocalPosition);
		}


		public bool AreOnTheSameSide(Vector3 first, Vector3 second)
		{
			float z = transform.position.z;

			return first.z < z && second.z < z || first.z > z && second.z > z;
		}


		public void Attach(Transform attachingTransform)
		{
			attachingTransform.SetParent(transform);
		}


		public void FreePusher(PushPlace pushPlace)
		{
			if (!pushPlace.IsTaken)
			{
				return;
			}
			
			bool isPlayer = IsPlaceForPlayer(pushPlace);
			
			int gameBalanceDelta = isPlayer ? -1 : 1;
			
			gameBalance += gameBalanceDelta;
		}


		public void AddPusher(PushPlace pushPlace)
		{
			bool isPlayer = IsPlaceForPlayer(pushPlace);
			int gameBalanceDelta = isPlayer ? 1 : -1;
			
			gameBalance += gameBalanceDelta;
		}

		#endregion
		


		#region Editor

		[Conditional("UNITY_EDITOR")]
		private void OnDrawGizmos()
		{
			foreach (List<PushPlace> pushPlaces in playerPlaces)
			{
				foreach (PushPlace pushPlace in pushPlaces)
				{
					Gizmos.DrawSphere(transform.position + pushPlace.LocalPosition, 0.3f);
				}
			}
			
			foreach (List<PushPlace> pushPlaces in enemyPlaces)
			{
				foreach (PushPlace pushPlace in pushPlaces)
				{
					Gizmos.DrawSphere(transform.position + pushPlace.LocalPosition, 0.3f);
				}
			}
		}

		#endregion
	}
}
